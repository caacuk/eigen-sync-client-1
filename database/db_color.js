const Sequelize = require("sequelize");
// require("dotenv/config");
const db_color = {};

const sequelize = new Sequelize("color", "new_user", "password", {
  host: "localhost",
  dialect: "mysql",
});

db_color.sequelize = sequelize;
db_color.Sequelize = Sequelize;

module.exports = db_color;
