const Sequelize = require("sequelize");
const db = {};
require("dotenv/config");

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: "mysql",
    dateStrings: true,
    timezone: "+07:00",
    logging: false,
  }
);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
