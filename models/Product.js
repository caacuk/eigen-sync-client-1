const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "product",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    code: {
      type: Sequelize.STRING,
    },
    product_type: {
      type: Sequelize.STRING,
    },
    name: {
      type: Sequelize.STRING,
    },
    hpp: {
      type: Sequelize.STRING,
    },
    material_price: {
      type: Sequelize.STRING,
    },
    sales_price: {
      type: Sequelize.STRING,
    },
    comission: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
    },
    category_id: {
      type: Sequelize.INTEGER,
    },
    unit_of_measurement_id: {
      type: Sequelize.INTEGER,
    },
    color_id: {
      type: Sequelize.INTEGER,
    },
    brand_id: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
