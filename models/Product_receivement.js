const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "product_receivement",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    type: {
      type: Sequelize.STRING,
    },
    branch_id: {
      type: Sequelize.INTEGER,
    },
    contact_id: {
      type: Sequelize.INTEGER,
    },
    code: {
      type: Sequelize.STRING,
    },
    date: {
      type: Sequelize.STRING,
    },
    reference: {
      type: Sequelize.STRING,
    },
    note: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
