const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "product_shipment",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    code: {
      type: Sequelize.STRING,
    },
    type: {
      type: Sequelize.STRING,
    },
    date: {
      type: Sequelize.DATE,
    },
    contact_id: {
      type: Sequelize.INTEGER,
    },
    branch_id: {
      type: Sequelize.INTEGER,
    },
    reference: {
      type: Sequelize.STRING,
    },
    note: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
