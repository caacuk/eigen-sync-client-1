const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "pos_item",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    product_id: {
      type: Sequelize.INTEGER,
    },
    uom_id: {
      type: Sequelize.INTEGER,
    },
    variant_type_id: {
      type: Sequelize.INTEGER,
    },
    pos_retur_id: {
      type: Sequelize.INTEGER,
    },
    promotion_id: {
      type: Sequelize.INTEGER,
    },
    qty: {
      type: Sequelize.STRING,
    },
    price: {
      type: Sequelize.STRING,
    },
    category: {
      type: Sequelize.STRING,
    },
    discount: {
      type: Sequelize.STRING,
    },
    total: {
      type: Sequelize.STRING,
    },
    proof_retur: {
      type: Sequelize.STRING,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
