const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "pos",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    code: {
      type: Sequelize.STRING,
    },
    member_id: {
      type: Sequelize.INTEGER,
    },
    branch_id: {
      type: Sequelize.INTEGER,
    },
    transaction_date: {
      type: Sequelize.DATE,
    },
    category: {
      type: Sequelize.STRING,
    },
    note: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
    },
    proof_member: {
      type: Sequelize.STRING,
    },
    payment_method: {
      type: Sequelize.STRING,
    },
    discount: {
      type: Sequelize.STRING,
    },
    total: {
      type: Sequelize.STRING,
    },
    ppn: {
      type: Sequelize.STRING,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
