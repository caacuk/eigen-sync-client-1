const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "promotion",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    code: {
      type: Sequelize.STRING,
    },
    name: {
      type: Sequelize.STRING,
    },
    type: {
      type: Sequelize.STRING,
    },
    brand_target: {
      type: Sequelize.STRING,
    },
    item_target: {
      type: Sequelize.STRING,
    },
    values: {
      type: Sequelize.STRING,
    },
    user_target: {
      type: Sequelize.STRING,
    },
    date_start: {
      type: Sequelize.DATE,
    },
    date_end: {
      type: Sequelize.DATE,
    },
    created_by_contact_id: {
      type: Sequelize.INTEGER,
    },
    updated_by_contact_id: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
