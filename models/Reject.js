const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "reject",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    branch_id: {
      type: Sequelize.INTEGER,
    },
    code: {
      type: Sequelize.STRING,
    },
    date: {
      type: Sequelize.DATE,
    },
    reference: {
      type: Sequelize.STRING,
    },
    notes: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
