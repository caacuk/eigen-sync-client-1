const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "stock_opname",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    code: {
      type: Sequelize.STRING,
    },
    branch_id: {
      type: Sequelize.INTEGER,
    },
    date: {
      type: Sequelize.DATE,
    },
    reference: {
      type: Sequelize.STRING,
    },
    note: {
      type: Sequelize.STRING,
    },
    based_on: {
      type: Sequelize.STRING,
    },
    location: {
      type: Sequelize.STRING,
    },
    warehouse_id: {
      type: Sequelize.INTEGER,
    },
    result: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
