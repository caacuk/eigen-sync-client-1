const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "sales_order",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    contact_id: {
      type: Sequelize.INTEGER,
    },
    code: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
    },
    shipment_date: {
      type: Sequelize.STRING,
    },
    date: {
      type: Sequelize.DATE,
    },
    note: {
      type: Sequelize.STRING,
    },
    created_by_contact_id: {
      type: Sequelize.INTEGER,
    },
    updated_by_contact_id: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
