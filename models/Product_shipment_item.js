const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "product_shipment_item",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    product_shipment_id: {
      type: Sequelize.INTEGER,
    },
    product_id: {
      type: Sequelize.INTEGER,
    },
    type: {
      type: Sequelize.STRING,
    },
    variant_id: {
      type: Sequelize.INTEGER,
    },
    qty: {
      type: Sequelize.STRING,
    },
    qty_shipment: {
      type: Sequelize.STRING,
    },
    uom_id: {
      type: Sequelize.INTEGER,
    },
    price: {
      type: Sequelize.STRING,
    },
    product_location: {
      type: Sequelize.STRING,
    },
    variant_type_id: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
