const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "payment",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    contact_id: {
      type: Sequelize.INTEGER,
    },
    payment_method: {
      type: Sequelize.STRING,
    },
    code: {
      type: Sequelize.STRING,
    },
    date: {
      type: Sequelize.STRING,
    },
    note: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.STRING,
    },
    created_by_contact_id: {
      type: Sequelize.STRING,
    },
    updated_by_contact_id: {
      type: Sequelize.STRING,
    },
    confirmed_by_contact_id: {
      type: Sequelize.STRING,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
