const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "member",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.STRING,
    },
    member_code: {
      type: Sequelize.STRING,
    },
    address: {
      type: Sequelize.STRING,
    },
    country: {
      type: Sequelize.STRING,
    },
    province: {
      type: Sequelize.STRING,
    },
    city: {
      type: Sequelize.STRING,
    },
    email: {
      type: Sequelize.STRING,
    },
    phone: {
      type: Sequelize.STRING,
    },
    postal_code: {
      type: Sequelize.STRING,
    },
    note: {
      type: Sequelize.STRING,
    },
    member_social: {
      type: Sequelize.STRING,
    },
    member_card_status: {
      type: Sequelize.STRING,
    },
    member_card_received_date: {
      type: Sequelize.STRING,
    },
    member_card_expired_date: {
      type: Sequelize.STRING,
    },
    is_active: {
      type: Sequelize.STRING,
    },
    total_transaction: {
      type: Sequelize.STRING,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
