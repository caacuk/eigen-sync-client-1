const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "stock_opname_item",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    stock_opname_id: {
      type: Sequelize.INTEGER,
    },
    category_id: {
      type: Sequelize.INTEGER,
    },
    category_name: {
      type: Sequelize.STRING,
    },
    code: {
      type: Sequelize.STRING,
    },
    product_id: {
      type: Sequelize.INTEGER,
    },
    product_name: {
      type: Sequelize.STRING,
    },
    brand_id: {
      type: Sequelize.INTEGER,
    },
    brand_name: {
      type: Sequelize.STRING,
    },
    product_qty: {
      type: Sequelize.INTEGER,
    },
    product_qty_physic: {
      type: Sequelize.INTEGER,
    },
    uom_id: {
      type: Sequelize.INTEGER,
    },
    uom_name: {
      type: Sequelize.STRING,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
