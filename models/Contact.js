const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "contact",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    type: {
      type: Sequelize.STRING,
    },
    is_active: {
      type: Sequelize.STRING,
    },
    name: {
      type: Sequelize.STRING,
    },
    code: {
      type: Sequelize.STRING,
    },
    phone: {
      type: Sequelize.STRING,
    },
    address: {
      type: Sequelize.STRING,
    },
    country: {
      type: Sequelize.STRING,
    },
    province: {
      type: Sequelize.STRING,
    },
    city: {
      type: Sequelize.STRING,
    },
    email: {
      type: Sequelize.STRING,
    },
    postal_code: {
      type: Sequelize.STRING,
    },
    note: {
      type: Sequelize.STRING,
    },
    is_brand: {
      type: Sequelize.STRING,
    },
    brand_comission_type: {
      type: Sequelize.STRING,
    },
    brand_comission: {
      type: Sequelize.STRING,
    },
    is_consigment: {
      type: Sequelize.STRING,
    },
    consigment_comission_type: {
      type: Sequelize.STRING,
    },
    consigment_comission: {
      type: Sequelize.STRING,
    },
    is_supplier: {
      type: Sequelize.STRING,
    },
    is_user: {
      type: Sequelize.STRING,
    },
    user_id: {
      type: Sequelize.STRING,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    },
    deleted_at: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    freezeTableName: true,
  }
);
