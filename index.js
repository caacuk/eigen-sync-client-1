require("dotenv/config");
const express = require("express");
const app = express();
const port = 3000;

// Socket client setup
const io = require("socket.io-client");
const socket = io(process.env.MASTER_URL, {
  transports: ["websocket"],
  reconnectionDelayMax: 10000,
  // query: {
  //   auth: "123",
  // },
});

// ======================================
const getData = require("./functions/getData");
const getAll = require("./functions/getAll");
const sendDataAfterDisconnected = require("./functions/sendDataAfterDisconnected");

// Synchronization Functions
const sync = require("./functions/sync");

let syncTime = new Date(Date.now());
let time = 5000; // 3sec
let dataTest = [];
const tables = [
  "Branch",
  "Brand",
  "Brand_category_item",
  "Category",
  "Category_variant",
  "Color",
  "Company",
  "Contact",
  "Edc",
  "Edc_item",
  "Invoice",
  "Invoice_item",
  "Member",
  "Payment",
  "Payment_item",
  "Pos",
  "Pos_item",
  "Product",
  "Product_receivement",
  "Product_receivement_item",
  "Product_shipment",
  "Product_shipment_item",
  "Product_variant_item",
  "Promotion",
  "Promotion_brand",
  "Promotion_category",
  "Promotion_product",
  "Reject",
  "Reject_item",
  "Sales_order",
  "Sales_order_product",
  "Sales_order_shipment",
  "Sales_return",
  "Sales_return_product",
  "Sales_return_shipment",
  "Stock_opname",
  "Stock_opname_category",
  "Stock_opname_item",
  "Stock_opname_product",
  "Unit",
  "User",
  "User_category",
  "Variant",
  "Variant_type",
  "Warehouse",
  "Warehouse_product",
  "Test",
];
const writeTables = ["Test"];

// Initial Data (Get All Data) Sync
const initialData = async (socket) => {
  if (writeTables.length > 0) {
    writeTables.map(async (table) => {
      initData = await getAll(models[table]);
      console.log(JSON.stringify(initData));
      socket.emit("init" + table, initData);
    });
  } else {
    socket.emit("init");
  }
};

// Models
const models = [];
tables.map((table) => {
  dataTest[table] = [];
  models[table] = require("./models/" + table);
});

let syncTimeStartB1 = new Date(Date.now());
let sendToServer_timer;
let firstConnected = true;

socket.on("connect", async function () {
  if (firstConnected) {
    console.log("initialData");
    initialData(socket);
  } else {
    console.log("sendDataAfterDisconnected");
    writeTables.map(async (table) => {
      await sendDataAfterDisconnected(
        socket,
        table,
        models[table],
        syncTimeStartB1
      );
    });
  }
  firstConnected = false;

  tables.map((table) => {
    socket.on(table, async function (newData) {
      try {
        await sync(newData, models[table]);
      } catch (err) {
        console.log(err);
      }
    });
  });

  console.log("connected " + syncTimeStartB1);
  await sendToServer(socket, time);
});

socket.on("disconnect", function () {
  syncTimeStartB1 = new Date(Date.now() - time); // Save time when disconnect
  clearTimeout(sendToServer_timer);
  console.log("disconnected " + syncTimeStartB1);
});

async function sendToServer(socket, time) {
  writeTables.map((table) => {
    if (dataTest[table].length > 0) {
      console.log("sendData to server");
      socket.emit(table, dataTest[table], syncTime);
      dataTest[table] = [];
      dataTemp = [];
    }
  });
  sendToServer_timer = setTimeout(function () {
    sendToServer(socket, time);
  }, time);
}

writeTables.map((table) => {
  setTimeout(function () {
    getDataEveryTime(time);
  }, time);

  let dataTemp = [];
  async function getDataEveryTime(time) {
    dataTemp = await getData(models[table], syncTime);
    dataTemp.map((x) => dataTest[table].push(x));

    console.log(
      getTimeNow() + " " + table + " ready : " + JSON.stringify(dataTest[table])
    );

    getDataEveryTime_timer = setTimeout(function () {
      getDataEveryTime(time);
      syncTime = new Date(Date.now());
    }, time);
  }
});

function getTimeNow() {
  const m = new Date();
  const dateString =
    "[" +
    m.getFullYear() +
    "/" +
    ("0" + (m.getMonth() + 1)).slice(-2) +
    "/" +
    ("0" + m.getDate()).slice(-2) +
    " " +
    ("0" + m.getHours()).slice(-2) +
    ":" +
    ("0" + m.getMinutes()).slice(-2) +
    ":" +
    ("0" + m.getSeconds()).slice(-2) +
    "]";
  return dateString;
}

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
