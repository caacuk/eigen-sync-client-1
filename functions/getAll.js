async function getAll(Model) {
  return await Model.findAll({
    raw: true,
  });
}

module.exports = getAll;
