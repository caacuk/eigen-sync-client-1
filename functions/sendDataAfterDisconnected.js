const getData = require("./getData");

async function sendDataAfterDisconnected(socket, table, Model, time) {
  // console.log(Date(Date.now()).toString());
  // console.log();

  const data = await getData(Model, time);
  console.log("ready : " + JSON.stringify(data));

  if (data.length > 0) {
    console.log("syncAfterDisconnect");
    socket.emit(table, data, time);
  }
}

module.exports = sendDataAfterDisconnected;
