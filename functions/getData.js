// Sequelize
const { Op } = require("sequelize");

async function getData(Model, syncTimeStart) {
  return await Model.findAll({
    raw: true,
    where: {
      [Op.or]: [
        {
          created_at: {
            [Op.between]: [syncTimeStart, new Date(Date.now())],
          },
        },
        {
          updated_at: {
            [Op.between]: [syncTimeStart, new Date(Date.now())],
          },
        },
      ],
    },
  });
}

module.exports = getData;
