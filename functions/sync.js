// Deep Diff JSON
const observableDiff = require("deep-diff").observableDiff;
const applyChange = require("deep-diff").applyChange;
const { Op } = require("sequelize");

async function sync(newData, Model) {
  // console.log(newData);
  // Get ID from newData
  const arrID = newData.map((x) => x.id);

  // Find by arrID in local database
  let localData = await Model.findAll({
    raw: true,
    where: {
      id: arrID,
    },
  });

  let status = "up to date";

  // Convert string to datetime
  for (i = 0; i < newData.length; i++) {
    newData[i].created_at = new Date(newData[i].created_at);
    newData[i].updated_at = new Date(newData[i].updated_at);
  }

  // Get difference property for update or insert new data
  observableDiff(localData, newData, function (d) {
    applyChange(localData, newData, d);
    status = "diff";
  });

  // If data is difference, do sync
  if (status !== "up to date") {
    localData.map(async (x) => {
      const id = x.id;
      const data = x;

      if (x.code) {
        try {
          const existCode = await Model.findOne({
            where: {
              code: x.code,
            },
          });

          if (existCode) {
            if (existCode.code === x.code && existCode.id === x.id) {
            } else {
              await existCode.destroy();
            }
          }
        } catch (err) {
          console.log(err);
        }
      }

      // Check ID
      const exist = await Model.findOne({
        where: {
          id: id,
        },
      });

      if (!exist) {
        // Insert
        await Model.create(data);
        console.log("add : " + JSON.stringify(data));
      } else {
        // Update
        await Model.update(data, {
          where: {
            id: id,
          },
        });
        console.log("put : " + JSON.stringify(data));
      }
    });
  }
}

module.exports = sync;
